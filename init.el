(add-to-list 'load-path "~/.emacs.d/straight/build/org")
(require 'org)
(org-babel-load-file
 (expand-file-name "config.org" user-emacs-directory))
